FROM bitnami/minideb:buster
MAINTAINER jpsecher@gmail.com

RUN install_packages ffmpeg wget bzip2 cifs-utils

ENV ROON_DATAROOT=/var/roon
ENV ROON_ID_DIR=/var/roon

RUN groupadd -g 1001 roon \
 	&& useradd -u 1001 -g roon -G plugdev -d /opt/RoonServer -r -s /usr/sbin/nologin roon \
 	&& mkdir $ROON_DATAROOT \
 	&& chown roon:roon $ROON_DATAROOT

# TODO: embed the Roonlabs SSL certificate
RUN wget -q --no-check-certificate -O- https://download.roonlabs.com/builds/RoonServer_linuxx64.tar.bz2 \
 	| tar xjf - -C /opt

VOLUME ["$ROON_DATAROOT", "/music"]

USER roon

RUN /opt/RoonServer/check.sh

ENTRYPOINT /opt/RoonServer/start.sh
