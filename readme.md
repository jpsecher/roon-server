# Roon Server in a Docker container

Start by downloading the container on your Linux box:

    $ docker pull jpsecher/roon-server

## Run

Let's assume that your music files are located under `~/Music`.  Then you can run the container directly thus:

    $ docker volume create roon-data
    $ docker run -d --net=host -v roon-data:/var/roon \
      -v ~/Music:/music \
      --name roonsrv \
      jpsecher/roon-server

If you want to start it in a Docker Swarm (recommended), use something like:

    version: "3.5"
    services:
      roon-server:
        image: jpsecher/roon-server
        volumes:
          - roon-data:/var/roon
          - /home/jps/Music:/music
        networks:
          hostnet: {}
        deploy:
          restart_policy:
            condition: on-failure
    volumes:
      roon-data:
    networks:
      hostnet:
        external: true
        name: host

If you want to start it with `docker-compose`, use something like:

    ...
    network_mode: "host"

*Note: the container (specifically the host mode net) will not work in Docker Desktop* 

## Configuration

When the Roon Server is up and running, you should be able to find it on your network using the GUI of the regular Roon Client.  If not, you might have a firewall problem.

When setting up the Roon Server the first time, navigate to the `/music` directory (that has been mounted inside the container).  And that's it.

## Update

It seems that the normal update procedure through the GUI works, but in case it does not, run

    $ docker pull jpsecher/roon-server
    $ docker stack deploy -c mycomposefile.yml myservice

If that fails, you can force a new image by:

    $ docker service update --image jpsecher/roon-server myservice_roon-server

For more info, see https://stackoverflow.com/a/58895994/13372

##  Build

    $ docker buildx build --pull --no-cache --platform=linux/amd64 -t jpsecher/roon-server .
    $ docker tag jpsecher/roon-server jpsecher/roon-server:1.8-763

----

[![Docker build status](https://img.shields.io/docker/cloud/build/jpsecher/roon-server.svg)](https://hub.docker.com/r/jpsecher/roon-server/builds/)
